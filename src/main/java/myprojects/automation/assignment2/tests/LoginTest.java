package myprojects.automation.assignment2.tests;

import myprojects.automation.assignment2.BaseScript;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.concurrent.TimeUnit;

public class LoginTest extends BaseScript {

    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = getDriver();
        driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
        WebElement login = driver.findElement(By.id("email"));
        login.sendKeys("webinar.test@gmail.com");
        WebElement password = driver.findElement(By.id("passwd"));
        password.sendKeys("Xcg7299bnSmMuRLp9ITw");
        WebElement loginbutton = driver.findElement(By.name("submitLogin"));
        loginbutton.click();
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        WebElement icon = driver.findElement(By.xpath("//*[@id=\"employee_infos\"]/a/span/img"));
        icon.click();
        WebElement logout = driver.findElement(By.id("header_logout"));
        logout.click();

    }
}

package myprojects.automation.assignment2.tests;

import myprojects.automation.assignment2.BaseScript;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.junit.Assert;

import java.util.List;
import java.util.concurrent.TimeUnit;



public class CheckMainMenuTest extends BaseScript {

    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = getDriver();
        driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
        WebElement login = driver.findElement(By.name("email"));
        login.sendKeys("webinar.test@gmail.com");
        WebElement password = driver.findElement(By.id("passwd"));
        password.sendKeys("Xcg7299bnSmMuRLp9ITw");
        WebElement loginbutton = driver.findElement(By.name("submitLogin"));
        loginbutton.click();
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);

//dashboard
        WebElement dashboard = driver.findElement(By.xpath("//*[@id=\"tab-AdminDashboard\"]/a/span"));
        dashboard.click();
        String dash = driver.getTitle();
        System.out.println(dash);
        driver.navigate().refresh();
        Assert.assertEquals(dash, driver.getTitle());
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
//закази
        WebElement order = driver.findElement(By.xpath("//*[@id=\"subtab-AdminParentOrders\"]/a/span"));
        order.click();
        String s = driver.getTitle();
        System.out.println(s);
        driver.navigate().refresh();
        Assert.assertEquals(s, driver.getTitle());
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
//каталог
        WebElement catalog = driver.findElement(By.xpath("//*[@id=\"subtab-AdminCatalog\"]/a/span"));
        catalog.click();
        String s1 = driver.getTitle();
        System.out.println(s1);
        driver.navigate().refresh();
        Assert.assertEquals(s1, driver.getTitle());
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
//клиенти
        WebElement support = driver.findElement(By.partialLinkText("Клиенты"));
        support.click();
        String s3 = driver.getTitle();
        System.out.println(s3);
        driver.navigate().refresh();
        Assert.assertEquals(s3, driver.getTitle());
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);

//служба поддержки
        WebElement customer = driver.findElement(By.partialLinkText("Служба поддержки"));
        customer.click();
        String s2 = driver.getTitle();
        System.out.println(s2);
        driver.navigate().refresh();
        Assert.assertEquals(s2, driver.getTitle());
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
//статистика
        WebElement stat = driver.findElement(By.xpath("//*[@id=\"subtab-AdminStats\"]/a/span"));
        stat.click();
        String s4 = driver.getTitle();
        System.out.println(s4);
        driver.navigate().refresh();
        Assert.assertEquals(s4, driver.getTitle());
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
//Modules
        WebElement modules = driver.findElement(By.xpath("//*[@id=\"subtab-AdminParentModulesSf\"]/a/span"));
        modules.click();
        String s5 = driver.getTitle();
        System.out.println(s5);
        driver.navigate().refresh();
        Assert.assertEquals(s5, driver.getTitle());
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
//Design
        WebElement design = driver.findElement(By.partialLinkText("Design"));
        design.click();
        String s6 = driver.getTitle();
        System.out.println(s6);
        driver.navigate().refresh();
        Assert.assertEquals(s6, driver.getTitle());
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
//Доставка
        WebElement delivery = driver.findElement(By.xpath("//*[@id=\"subtab-AdminParentShipping\"]/a/span"));
        delivery.click();
        String s7 = driver.getTitle();
        System.out.println(s7);
        driver.navigate().refresh();
        Assert.assertEquals(s7, driver.getTitle());
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
// Способ оплати
        WebElement payment = driver.findElement(By.xpath("//*[@id=\"subtab-AdminParentPayment\"]/a/span"));
        payment.click();
        String s8 = driver.getTitle();
        System.out.println(s8);
        driver.navigate().refresh();
        Assert.assertEquals(s8, driver.getTitle());
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
//International
        WebElement international = driver.findElement(By.xpath("//*[@id=\"subtab-AdminInternational\"]/a/span"));
        international.click();
        String s9 = driver.getTitle();
        System.out.println(s9);
        driver.navigate().refresh();
        Assert.assertEquals(s9, driver.getTitle());
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
//Shop Parameters
        WebElement shop = driver.findElement(By.xpath("//*[@id=\"subtab-ShopParameters\"]/a/span"));
        shop.click();
        String a = driver.getTitle();
        System.out.println(a);
        driver.navigate().refresh();
        Assert.assertEquals(a, driver.getTitle());
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
//Конфигурации
        WebElement adminparametrs = driver.findElement(By.xpath("//*[@id=\"subtab-AdminAdvancedParameters\"]/a/span"));
        adminparametrs.click();
        String a1 = driver.getTitle();
        System.out.println(a1);
        driver.navigate().refresh();
        Assert.assertEquals(a1, driver.getTitle());
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);

    }
}
